<?php

global $_MODULE;
$_MODULE = array();
$_MODULE['<{authorizedsp}prestashop>authorizedsp_df5c3255039cf35a6ed030a4ebab7b59'] = 'Authorize.Net DSP (Deep South Production)';
$_MODULE['<{authorizedsp}prestashop>authorizedsp_5b3b16cd2bbbec8b2c282750241870ad'] = 'Permite recibir pagos por tarjeta';
$_MODULE['<{authorizedsp}prestashop>authorizedsp_e09484ba6c16bc20236b63cc0d87ee95'] = 'Estas seguro de querer borrar este detalle?';
$_MODULE['<{authorizedsp}prestashop>authorizedsp_8755287bd4fae263927768401b204bd7'] = 'El campo \"Log ID\" es requerido.';
$_MODULE['<{authorizedsp}prestashop>authorizedsp_4f92d1fec3301d608b64dff87af8887a'] = 'El campo \"Key\" es requerido.';
$_MODULE['<{authorizedsp}prestashop>authorizedsp_c888438d14855d7d96a2724ee9c306bd'] = 'Configuración Actualizada';
$_MODULE['<{authorizedsp}prestashop>authorizedsp_2471e6ef6ef9018973f1658492115b52'] = 'Pagar con tarjeta';
$_MODULE['<{authorizedsp}prestashop>authorizedsp_86261c33e8812d1b9060077c53073c4c'] = 'Credenciales Authorize.net';
$_MODULE['<{authorizedsp}prestashop>authorizedsp_b5b0107e400873925cdd913c0b791401'] = 'Llave secreta para las transacciones';
$_MODULE['<{authorizedsp}prestashop>authorizedsp_c9cc8cce247e49bae79f15173ce97354'] = 'Guardar';
$_MODULE['<{authorizedsp}prestashop>validation_e2b7dec8fa4b498156dfee6e4c84b156'] = 'Este método de pago no esta disponible ';
$_MODULE['<{authorizedsp}prestashop>payment_infos_b915f969803d97ebae924ef447c87932'] = 'Información de Tarjeta';
$_MODULE['<{authorizedsp}prestashop>payment_infos_a6c964bb8de0b00c3ec68a210e877da0'] = 'Nombre en Tarjeta';
$_MODULE['<{authorizedsp}prestashop>payment_infos_931d3a3ad177dd96a28c9642fec11b01'] = 'Numero de Tarjeta';
$_MODULE['<{authorizedsp}prestashop>payment_infos_958e0d1cb84aa93db31ce2e47d431dbe'] = 'Expira';
$_MODULE['<{authorizedsp}prestashop>payment_infos_60a104dc50579d60cbc90158fada1dcf'] = 'cvv';
