<p>
    <h3>{l s='Your order on %s is complete.' sprintf=[$shop_name] mod='authorizedsp'}</h3>
    <br /><br />
    {l s='Order # %s paid.' sprintf=[$id_order] mod='authorizedsp'}<br />
    {l s='Payment # %s process.' sprintf=[$reference] mod='authorizedsp'}<br />
    <br /><br />{l s='For any questions or for further information, please contact our' mod='authorizedsp'} <a href="{$link->getPageLink('contact', true)|escape:'html'}">{l s='customer service department.' mod='authorizedsp'}</a>.
</p>