<div class="alert alert-info">
<p><strong>{l s="This module allows you to accept payments by card." mod='authorizedsp'}</strong></p>
<p>{l s="If the client chooses this payment method, the order status will change to 'Waiting for payment'." mod='authorizedsp'}</p>
<p>{l s="You will need to manually confirm the order as soon as you receive a check." mod='authorizedsp'}</p>
</div>
