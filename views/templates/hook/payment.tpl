<p class="payment_module">
	<a href="{$link->getModuleLink('authorizedsp', 'payment', [], true)|escape:'html'}">
		<img src="{$this_path_authorizedsp}logo.jpg" alt="{l s='Pay by Card' d='Modules.authorizedsp.Shop'}" />
		{l s='Pay by Card' d='Modules.authorizedsp.Shop'} {l s='(order processing will be longer)' d='Modules.authorizedsp.Shop'}
	</a>
</p>
