<form action="{$action}" id="payment-form-authorizedsp" method="post">
  <div class="card-title m-1">
    <h5>{l s='Card Information' mod='authorizedsp'}</h5>
  </div>
  <div class="container mt-1">
    <div class="form-group">
      <label>*{l s="Card Name" mod='authorizedsp'}</label>
      <input class="form-control" name="card_name" id="card_name" required placeholder="{l s='Homero Adam' mod='authorizedsp'}"/>
      <small class="collapse text-danger">{l s='Please provide a valid card name.' mod='authorizedsp'}</small>
    </div>
    
    <div class="row">
      <div class="form-group col-sm-12 col-md-6">
        <label>*{l s='Card Number' mod='authorizedsp'}</label>
        <input class="form-control" name="card_number" id="card_number" required placeholder="{l s='4111 1111 1111 1111' mod='authorizedsp'}"/>
        <small class="collapse text-danger">{l s='Please provide a valid card number.' mod='authorizedsp'}</small>
      </div>
      <div class="form-group col-sm-12 col-md-4">
        <label>*{l s='Card Expire' mod='authorizedsp'}</label>
        <input class="form-control" name="card_expire" id="card_expire" required placeholder="{l s='MM/YYYY' mod='authorizedsp'}"/>
        <small class="collapse text-danger">{l s='Please provide a valid card date expire.' mod='authorizedsp'}</small>
      </div>
      <div class="form-group col-sm-12 col-md-2">
        <label>*{l s='CVC' mod='authorizedsp'}</label>
        <input class="form-control" name="card_cvv" id="card_cvv" required placeholder="{l s='000' mod='authorizedsp'}" />
        <small class="collapse text-danger">{l s='Please provide a valid card code.' mod='authorizedsp'}</small>
      </div>
    </div> 
  </div>
  <small>{l s='Fields with asterisks(*) are required' mod='authorizedsp'}</small>
  <div class="alert alert-danger collapse">{l s='Check the entered data' mod='authorizedsp'}</div>
</form>