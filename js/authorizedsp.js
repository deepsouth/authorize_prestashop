$.fn.CardForm = function() {
    var self = this;
    
    self.props = {
        number: self.find("#card_number"),
        expire: self.find("#card_expire"),
        cvv: self.find("#card_cvv"),
        name: self.find("#card_name")
    };

    self.getExpire = function() {
        var expire = self.props.expire.val(); 
        var parts = expire.split('/');
        if (parts.length == 2) {
            return {
                month: parts[0],
                year: parts[1]
            };
        }
        return false;
    };

    self.getCVV = function() {
        return self.props.cvv.val();
    };

    self.getNumber = function() {
        return self.props.number.val();
    };

    self.getName = function() {
        return self.props.name.val();
    };

    self.validateName = function() {
        var name = self.getName();
        return name && name.trim().length > 0; 
    }

    function __const(){
        self.props.number.payform('formatCardNumber');
        self.props.expire.payform('formatCardExpiry')
        self.props.cvv.payform('formatCardCVC');        
    }
    
    self.on('error-validation', function() {
        var expire = self.getExpire();
        var cvv = self.getCVV();
        var number = self.getNumber();
        if (!self.validateName()) {
            self.props.name.parent().addClass('has-error');
            self.props.name.parent().find('.text-danger').removeClass('collapse');
            self.props.name.addClass('is-invalid');
        } else {
            self.props.name.parent().removeClass('has-error');
            self.props.name.parent().find('.text-danger').addClass('collapse');
            self.props.name.removeClass('is-invalid');
        }

        if (!payform.validateCardExpiry(expire.month, expire.year)) {
            self.props.expire.parent().addClass('has-error');
            self.props.expire.parent().find('.text-danger').removeClass('collapse');
            self.props.expire.addClass('is-invalid');
        } else {
            self.props.expire.parent().removeClass('has-error');
            self.props.expire.parent().find('.text-danger').addClass('collapse');
            self.props.expire.removeClass('is-invalid');
        }
        if (!payform.validateCardCVC(cvv)) {
            self.props.cvv.parent().addClass('has-error');
            self.props.cvv.parent().find('.text-danger').removeClass('collapse');
            self.props.cvv.addClass('is-invalid');
        } else {
            self.props.cvv.parent().removeClass('has-error');
            self.props.cvv.parent().find('.text-danger').addClass('collapse');
            self.props.cvv.removeClass('is-invalid');
        }
        if (!payform.validateCardNumber(number)) {
            self.props.number.parent().addClass('has-error');
            self.props.number.parent().find('.text-danger').removeClass('collapse');
            self.props.number.addClass('is-invalid');
        } else {
            self.props.number.parent().removeClass('has-error');
            self.props.number.parent().find('.text-danger').addClass('collapse');
            self.props.number.removeClass('is-invalid');
        }
    })

    self.on('submit', function(evt) {
        var expire = self.getExpire();
        var cvv = self.getCVV();
        var number = self.getNumber();
        var validate = expire 
        && payform.validateCardExpiry(expire.month, expire.year) 
        && payform.validateCardCVC(cvv)
        && payform.validateCardNumber(number);
        if (!validate) {
            self.find('.alert').removeClass('collapse');
            self.trigger('error-validation');
        } else {
            self.find('.alert').addClass('collapse');
        }
        return validate;
    });

    __const();
    return self;
};


$('#payment-form-authorizedsp').CardForm();