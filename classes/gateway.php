<?php

require_once __DIR__.'/../vendor/autoload.php';

use net\authorize\api\contract\v1 as AnetAPI;
use net\authorize\api\controller as AnetController;

class AuthorizeGateway
{

    public $errors;
    protected function createCredential()
    {
        $logid = Configuration::get('AUTHDSP_LOGID');
        $key = Configuration::get('AUTHDSP_KEY');
        $auth = new AnetAPI\MerchantAuthenticationType();
        $auth->setName($logid);
        $auth->setTransactionKey($key);
        return $auth;
    }

    protected function createPayment($paymentInfo)
    {
        $creditCard = new AnetAPI\CreditCardType();
        $cardNumber = str_replace(' ', '', $paymentInfo['card_number']);
        $cardExpirePart = split('/', $paymentInfo['card_expire']);
        $cardExpire = str_replace(' ', '', $cardExpirePart[1]).'-'. str_replace(' ', '', $cardExpirePart[0]);
        $creditCard->setCardNumber($cardNumber);
        $creditCard->setExpirationDate($cardExpire);
        $creditCard->setCardCode($paymentInfo['card_cvv']);
        $paymentOne = new AnetAPI\PaymentType();
        $paymentOne->setCreditCard($creditCard);
        return $paymentOne;
    
    } 

    protected function createOrder($cart)
    {
        $order = new AnetAPI\OrderType();
        $nro = 'C'.str_pad($cart->id, 5, "0", STR_PAD_LEFT);
        $order->setInvoiceNumber($nro);
        $order->setDescription("Purchase Online");
        return $order;
    }

    protected function createTransaction($amount)
    {
        $transactionRequestType = new AnetAPI\TransactionRequestType();
        $transactionRequestType->setTransactionType("authOnlyTransaction"); 
        $transactionRequestType->setAmount(round($amount, 2));
        return $transactionRequestType;

    }

    protected function createLines($products)
    {   
        $res = [];
        foreach($products as $product){
            $lineItem = new AnetAPI\LineItemType();
            $lineItem->setItemId($product['id_product']);
            $lineItem->setName(substr($product['name'],0,31));
            $lineItem->setDescription(substr($product['description_short'],0,255));
            $lineItem->setQuantity($product['cart_quantity']);
            $lineItem->setUnitPrice(round($product['price'], 2));
            $res[] = $lineItem;
        }
        return $res;
    }

    protected function createAddress($customer, $address){
        $state = new State($address->id_state);
        $country = new Country($address->id_country);
        $customerAddress = new AnetAPI\CustomerAddressType();
        $customerAddress->setFirstName($customer->firstname);
        $customerAddress->setLastName($customer->lastname);
        $customerAddress->setEmail($customer->email);
        $customerAddress->setCompany($customer->company);
        $customerAddress->setAddress($address->address1);
        $customerAddress->setCity($address->city);
        $customerAddress->setState($state->iso_code);
        $customerAddress->setZip($address->postcode);
        $customerAddress->setCountry($country->iso_code);
        return $customerAddress;
    }

    public function doPay($paymentInfo, $params)
    {
        $payment = $this->createPayment($paymentInfo);
        $customer = $this->createAddress($params['customer'], $params['address']);
        $order = $this->createOrder($params['cart']);
        $lines = $this->createLines($params['products']);


        $duplicateWindowSetting = new AnetAPI\SettingType();
        $duplicateWindowSetting->setSettingName("duplicateWindow");
        $duplicateWindowSetting->setSettingValue("60");

        $transaction = $this->createTransaction($params['amount']);
        $transaction->setOrder($order);
        $transaction->setPayment($payment);
        $transaction->setBillTo($customer);
        $transaction->setLineItems($lines);
        $transaction->setTerminalNumber('DSP001');
        $refId = 'DSP-'.$params['cart']->id;
        $transaction->addToTransactionSettings($duplicateWindowSetting);
        
        $request = new AnetAPI\CreateTransactionRequest();
        $request->setMerchantAuthentication($this->createCredential());
        $request->setRefId($refId);
        $request->setTransactionRequest($transaction);
    
        $controller = new AnetController\CreateTransactionController($request);
        $env = Configuration::get('AUTHDSP_ENV') == 1 ? \net\authorize\api\constants\ANetEnvironment::PRODUCTION : \net\authorize\api\constants\ANetEnvironment::SANDBOX;
        $response = $controller->executeWithApiResponse($env);
        if (!$response) {
            $this->errors = ['code' => 'E00666', 'Error comunication provider the gateway payment'];
            return false;
        }
        if ($response->getMessages()->getResultCode() != "Error") {
            $tresponse = $response->getTransactionResponse();
            if ($tresponse != null && $tresponse->getMessages() != null) {
                return $tresponse->getTransId();
            } else {
                if ($tresponse->getErrors() != null) {
                    $this->errors = [
                        'code'=> $tresponse->getErrors()[0]->getErrorCode(),
                        'message'=> $tresponse->getErrors()[0]->getErrorText()];
                }   
                return false;
            }
        } else {
            $tresponse = $response->getTransactionResponse();
            if ($tresponse != null && $tresponse->getErrors() != null) {
                $this->errors = [
                    'code'=>$tresponse->getErrors()[0]->getErrorCode(),
                    'message'=>$tresponse->getErrors()[0]->getErrorText()
                ];
            } else {
                $message = $response->getMessages()->getMessage()[0];
                $this->errors = [
                    'code'=>$message->getCode(),
                    'message'=>$message->getText()
                ];
            }
            return false;
        }
        
    }
}