<?php

require_once __DIR__.'/../../classes/gateway.php';

class AuthorizedspValidationModuleFrontController extends ModuleFrontController
{

    public function postProcess()
    {
        $cart = $this->context->cart;

        if ($cart->id_customer == 0 || $cart->id_address_delivery == 0 || $cart->id_address_invoice == 0 || !$this->module->active) {
            Tools::redirect('index.php?controller=order&step=1');
        }

        $authorizedsp = Module::getInstanceByName('authorizedsp');
        $customer = new Customer($cart->id_customer);
        $address = new Address($cart->id_address_invoice);
        $products = $this->context->cart->getProducts(true);
        $total_to_pay = (float)$cart->getOrderTotal(true, Cart::BOTH);

        // Check that this payment option is still available in case the customer changed his address just before the end of the checkout process
        $authorized = false;
        foreach (Module::getPaymentModules() as $module) {
            if ($module['name'] == 'authorizedsp') {
                $authorized = true;
                break;
            }
        }

        if (!$authorized) {
            die($this->l('This payment method is not available.'));
        }

        $params =  $_REQUEST;
        $gateway = new \AuthorizeGateway();

        $order_status = (int)Configuration::get('PS_OS_PAYMENT');
        
        $transaction = $gateway->doPay($params, [
            'cart'=>$cart,
            'customer'=>$customer,
            'address'=>$address,
            'products'=>$products,
            'amount'=>$total_to_pay
        ]);
        if (!$transaction) {
            $this->errors[] = $gateway->errors['message'];
            Tools::redirect('index.php?controller=order&error_auth_dsp='.$gateway->errors['code']);
        } else {
            $state =  Configuration::get('PS_OS_PAYMENT');
            $currency = Context::getContext()->currency;
            $authorizedsp->validateOrder($cart->id, $state, $total_to_pay, 'Authorize.net Deep South Production', null, $transaction, (int)$currency->id, false, $customer->secure_key);
            Tools::redirect('index.php?controller=order-confirmation&id_cart='.$cart->id.'&id_module='.$authorizedsp->id.'&id_order='.$authorizedsp->currentOrder.'&key='.$customer->secure_key);
        }
    }


}
