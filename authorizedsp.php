<?php

use PrestaShop\PrestaShop\Core\Payment\PaymentOption;

if (!defined('_PS_VERSION_')) {
    exit;
}

require_once __DIR__.'/errors.php';

class Authorizedsp extends PaymentModule
{
    private $_html= '';
    private $_postErrors = array();
    
    private $logid;
    private $key;
    private $env;

    public function __construct()
    {
        $this->name = 'authorizedsp';
        $this->tab = 'payments_gateways';
        $this->version = '1.0.0';
        $this->author = 'Yan';
        $this->controllers = array('payment', 'validation');

        $this->currencies = true;
        $this->currencies_mode = 'checkbox';

        $config = Configuration::getMultiple(array('AUTHDSP_LOGID', 'AUTHDSP_KEY', 'AUTHDSP_ENV'));
        if (isset($config['AUTHDSP_LOGID'])) {
            $this->logid = $config['AUTHDSP_LOGID'];
        }
        if (isset($config['AUTHDSP_KEY'])) {
            $this->key = $config['AUTHDSP_KEY'];
        }
        if (isset($config['AUTHDSP_ENV'])) {
            $this->env = $config['AUTHDSP_ENV'];
        }

        $this->bootstrap = true;
        parent::__construct();

        $this->displayName = $this->l('Authorize.Net DSP (Deep South Production)');
        $this->description = $this->l('Allows you to accept payments by Card.');
        $this->confirmUninstall = $this->l('Are you sure you want to delete these details?');
        $this->ps_versions_compliancy = array('min' => '1.7.1.0', 'max' => _PS_VERSION_);

        if ((!isset($this->logid) || !isset($this->key) || empty($this->logid) || empty($this->key))) {
            $this->warning = $this->trans('The "Log Id" and "Key" fields must be configured before using this module.', array(), 'Modules.authorizedsp.Admin');
        }
        if (!count(Currency::checkPaymentCurrencies($this->id))) {
            $this->warning = $this->trans('No currency has been set for this module.', array(), 'Modules.authorizedsp.Admin');
        }

        $this->extra_mail_vars = array(
                                    '{auth_logid}' => Configuration::get('AUTHDSP_LOGID'),
                                    '{auth_key}' => Configuration::get('AUTHDSP_KEY'),
                                    '{auth_env}' => Configuration::get('AUTHDSP_ENV')
                                );
    }

    public function install()
    {
        return parent::install()
        && $this->installOrderState()
        && $this->registerHook('displayHeader') 
        && $this->registerHook('paymentOptions')
        && $this->registerHook('paymentReturn')
        ;
    }
    
    public function installOrderState()
    {
        if (!Configuration::get('AUTHORIZEDSP_OS_WAITING')
            || !Validate::isLoadedObject(new OrderState(Configuration::get('AUTHORIZEDSP_OS_WAITING')))) {
            $order_state = new OrderState();
            $order_state->name = array();
            foreach (Language::getLanguages() as $language) {
                if (Tools::strtolower($language['iso_code']) == 'es') {
                    $order_state->name[$language['id_lang']] = 'En espera de pago por Authorize.Net';
                } else {
                    $order_state->name[$language['id_lang']] = 'Awaiting for Authorize.Net payment';
                }
            }
            $order_state->send_email = false;
            $order_state->color = '#00264d';
            $order_state->hidden = false;
            $order_state->delivery = false;
            $order_state->logable = false;
            $order_state->invoice = false;
            if ($order_state->add()) {
                $source = _PS_MODULE_DIR_.'authorizedsp/logo.png';
                $destination = _PS_ROOT_DIR_.'/img/os/'.(int) $order_state->id.'.gif';
                copy($source, $destination);
            }

            Configuration::updateValue('AUTHORIZEDSP_OS_WAITING', (int) $order_state->id);
        }
        return true;
    }

    public function unistall()
    {
        return Configuration::deleteByName('AUTHDSP_LOGID')
        && Configuration::deleteByName('AUTHDSP_KEY')
        && Configuration::deleteByName('AUTHDSP_ENV')
        && parent::uninstall()
        ;
    }

    private function _postValidation()
    {
        if (Tools::isSubmit('btnSubmit')) {
            if (!Tools::getValue('AUTHDSP_LOGID')) {
                $this->_postErrors[] = $this->l('The "Log ID" field is required.');
            } elseif (!Tools::getValue('AUTHDSP_KEY')) {
                $this->_postErrors[] = $this->l('The "Key" field is required.');
            } elseif (!Tools::getValue('AUTHDSP_ENV')) {
                $this->_postErrors[] = $this->l('The "Env" field is required.');
            }
        }
    }

    private function _postProcess()
    {
        if (Tools::isSubmit('btnSubmit')) {
            Configuration::updateValue('AUTHDSP_LOGID', Tools::getValue('AUTHDSP_LOGID'));
            Configuration::updateValue('AUTHDSP_KEY', Tools::getValue('AUTHDSP_KEY'));
            Configuration::updateValue('AUTHDSP_ENV', Tools::getValue('AUTHDSP_ENV'));
        }
        $this->_html .= $this->displayConfirmation($this->l('Settings updated'));
    }

    private function _displayCheck()
    {
        return $this->display(__FILE__, './views/templates/hook/infos.tpl');
    }

    public function getContent()
    {
        $this->_html = '';

        if (Tools::isSubmit('btnSubmit')) {
            $this->_postValidation();
            if (!count($this->_postErrors)) {
                $this->_postProcess();
            } else {
                foreach ($this->_postErrors as $err) {
                    $this->_html .= $this->displayError($err);
                }
            }
        }

        $this->_html .= $this->_displayCheck();
        $this->_html .= $this->renderForm();

        return $this->_html;
    }

    private function _displayForm()
    {
        $helper = new HelperForm();
    }

    private function registerJs()
    {
        $this->context->controller->registerJavascript(
            'payform', 
            'modules/'.$this->name."/js/lib/payform.min.js",
            ['position' => 'bottom', 'priority' => 150]);
        $this->context->controller->registerJavascript(
            'jquery.payform', 
            'modules/'.$this->name."/js/lib/jquery.payform.min.js",
            ['position' => 'bottom', 'priority' => 151]);
        $this->context->controller->registerJavascript(
            'authorizedsp', 
            'modules/'.$this->name."/js/authorizedsp.js",
            ['position' => 'bottom', 'priority' => 152]);
        if ($err = Tools::getValue('error_auth_dsp')) {
            $instance = new Errors($this);
            $controller = $this->context->controller;
            $err = $instance->getErrorDescription($err);
            $message = isset($err['description']) ? $err['description'] : $err['text'];
            $controller->errors[] = $message;
        }
        return false;
    }

    public function hookActionFrontControllerSetMedia($params)
    {
        $this->registerJs();
    }

    public function hookdisplayHeader($params)
    {
        $this->registerJs();
    }

    public function hookPaymentOptions($params)
    {
        if (!$this->active) {
            return;
        }
        if (!$this->checkCurrency($params['cart'])) {
            return;
        }

        
        $this->smarty->assign(
            $this->getTemplateVars()
        );
        $form = $this->generateForm();
        $newOption = new PaymentOption();
        $newOption->setModuleName($this->name)
                ->setCallToActionText($this->l('Pay by Card'))
                ->setForm($form);

        return [$newOption];
    }

    public function hookPaymentReturn($params)
    {
        if (!$this->active) {
            return;
        }
        $order = $params['order'];
        $payments = $order->getOrderPaymentCollection();
        $state = $params['order']->getCurrentState();
        $this->smarty->assign(array(
            'total_to_pay' => Tools::displayPrice(
                $params['order']->getOrdersTotalPaid(),
                new Currency($params['order']->id_currency),
                false
            ),
            'shop_name' => $this->context->shop->name,
            'id_order' => $params['order']->id
        ));
        if (isset($payments) && !empty($payments)) {
            $this->smarty->assign('reference', $payments[0]->transaction_id);
        }
        
        return $this->fetch('module:authorizedsp/views/templates/hook/payment_return.tpl');
    }

    public function checkCurrency($cart)
    {
        $currency_order = new Currency((int)($cart->id_currency));
        $currencies_module = $this->getCurrency((int)$cart->id_currency);

        if (is_array($currencies_module)) {
            foreach ($currencies_module as $currency_module) {
                if ($currency_order->id == $currency_module['id_currency']) {
                    return true;
                }
            }
        }
        return false;
    }

    public function renderForm()
    {
        $options = array(
            array(
                'id_option' => 1,                 // The value of the 'value' attribute of the <option> tag.
                'name' => 'Production'              // The value of the text content of the  <option> tag.
            ),
            array(
                'id_option' => 2,
                'name' => 'Test'
            ),
        );
        $fields_form = array(
            'form' => array(
                'legend' => array(
                    'title' => $this->l('Credentials Authorize'),
                    'icon' => 'icon-envelope'
                ),
                'input' => array(
                    array(
                        'type' => 'text',
                        'label' => $this->l('Log Id'),
                        'name' => 'AUTHDSP_LOGID',
                        'required' => true
                    ),
                    array(
                        'type' => 'textarea',
                        'label' => $this->l('Key'),
                        'desc' => $this->l('Key secret to transactions authorize.'),
                        'name' => 'AUTHDSP_KEY',
                        'required' => true
                    ),
                    array(
                        'type' => 'select',
                        'label' => $this->l('Mode'),
                        'desc'    => $this->l('Choose options.'),
                        'name' => 'AUTHDSP_ENV',
                        'required' => true,
                        'options'  => array(
                            'query' => $options,                           // $options contains the data itself.
                            'id'    => 'id_option',                        // The value of the 'id' key must be the same as the key for 'value' attribute of the <option> tag in each $options sub-array.
                            'name'  => 'name'                              // The value of the 'name' key must be the same as the key for the text content of the <option> tag in each $options sub-array.
                        ),
                    ),
                ),
                'submit' => array(
                    'title' => $this->l('Save'),
                )
            ),
        );

        $helper = new HelperForm();
        $helper->show_toolbar = false;
        $helper->id = (int)Tools::getValue('id_carrier');
        $helper->identifier = $this->identifier;
        $helper->submit_action = 'btnSubmit';
        $helper->currentIndex = $this->context->link->getAdminLink('AdminModules', false).'&configure='.$this->name.'&tab_module='.$this->tab.'&module_name='.$this->name;
        $helper->token = Tools::getAdminTokenLite('AdminModules');
        $helper->tpl_vars = array(
            'fields_value' => $this->getConfigFieldsValues(),
        );

        $this->fields_form = array();

        return $helper->generateForm(array($fields_form));
    }

    public function getConfigFieldsValues()
    {
        return array(
            'AUTHDSP_LOGID' => Tools::getValue('AUTHDSP_LOGID', Configuration::get('AUTHDSP_LOGID')),
            'AUTHDSP_KEY' => Tools::getValue('AUTHDSP_KEY', Configuration::get('AUTHDSP_KEY')),
            'AUTHDSP_ENV' => Tools::getValue('AUTHDSP_ENV', Configuration::get('AUTHDSP_ENV')),
        );
    }

    public function validateOrder($id_cart, $id_order_state, $amount_paid, $payment_method = 'Unknown', $message = null, $transaction = array(), $currency_special = null, $dont_touch_amount = false, $secure_key = false, Shop $shop = null)
    {
        parent::validateOrder(
            (int) $id_cart,
            (int) $id_order_state,
            (float) $amount_paid,
            $payment_method,
            $message,
            array('transaction_id' => $transaction),
            $currency_special,
            $dont_touch_amount,
            $secure_key,
            $shop
        );
    }

    public function getTemplateVars()
    {
        $cart = $this->context->cart;
        $amount = Tools::displayPrice($cart->getOrderTotal(true, Cart::BOTH));
        $total = "$amount ".$this->l("tax incl.");
        
        return [
            'total' => $total
        ];
    }

    protected function generateForm()
    {
        $this->context->smarty->assign([
            'action' => $this->context->link->getModuleLink($this->name, 'validation', array(), true)
        ]);
        return $this->context->smarty->fetch('module:'.$this->name.'/views/templates/front/payment_infos.tpl');
    }

}